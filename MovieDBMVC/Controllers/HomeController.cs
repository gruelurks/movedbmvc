﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MovieDBMVC.Models;

namespace MovieDBMVC.Controllers
{
  public class HomeController : Controller
  {
    /// <summary>Default view for the MovieDB application</summary>
    /// <returns>Index view</returns>
    public IActionResult Index()
    {
      return View();
    }


    /// <summary>Returns an error view</summary>
    /// <returns>ErrorVewModel</returns>
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
      return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
  }
}
